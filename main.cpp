#include <iostream>
#include <stdio.h>
#include <malloc.h>
#include <fstream>
#include <stdlib.h>
#include "DeformationGraph.h"

using namespace std;

int main() {
	ifstream infile_1;
	infile_1.open("in.txt");
	int n, m;
	infile_1 >> n >> m;
	Node *node = (Node *)malloc(10000*sizeof (double));
	for (int i = 0; i < n; i++) {
		infile_1 >> node[i][0] >> node[i][1] >> node[i][2];
	}
	double **vertices = (double**)malloc(50000*sizeof (double *));
	for (int i = 0; i < m; i++) {
		vertices[i] = (double*)malloc(10 * sizeof(double));
		infile_1 >> vertices[i][0] >> vertices[i][1] >> vertices[i][2];
	}
	DeformationGraph &dg = DeformationGraph(n,node, m,vertices,4);
	int p = 3216;
	infile_1.close();
	ifstream infile_2;
	infile_2.open("vertices_deformed.txt");
	double **q = (double**)malloc(50000*sizeof (double*));
	for (int i = 0; i < p; i++) {
		q[i] = (double*)malloc(10 * sizeof(double));
		infile_2 >>q[i][0] >> q[i][1] >>q[i][2];
	}
	ifstream infile_3;
	infile_3.open("vertices_tobe_deformed.txt");
	double **v = (double**)malloc(50000 * sizeof(double*));
	for (int i = 0; i < p; i++) {
		v[i] = (double*)malloc(10 * sizeof(double));
		infile_3 >> v[i][0] >> v[i][1] >> v[i][2];
	}
	printf("aa\n");
	gaussNewton(dg, p, v, q);
	//void gaussNewton(DeformationGraph &dg, int p, double **v, double **q)
	Show(dg, vertices,m);
	cout << "hello world" << endl;
	system("pause");
	return 0;
}